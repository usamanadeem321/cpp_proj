#include "../include/containers.h"
#include <algorithm>
#include <iostream>
using namespace std;

void abc::print_func() { std::cout << "Hello World" << std::endl; }
abc ::abc() {

  sr.max = 0;
  sr.min = 0;
  sr.size = 0;
  sr.SumOfSquares = 0;

  std::cout << "Constructor called" << std::endl;
  sum_of_squares = 0;
}
abc ::~abc() { std::cout << "DESTRUCTOR called" << std::endl; }

vector<int> abc::squared(vector<int> v) {
  cout << "Squaring VECTOR" << endl;
  for (int i = 0; i < v.size(); i++)
    v[i] = v[i] * v[i];

  return v;
}
list<int> abc::squared(list<int> l) {
  cout << "Squaring LIST" << endl;
  list<int>::iterator it;

  for (it = l.begin(); it != l.end(); it++)
    *it = (*it) * (*it);

  return l;
}

void abc::disp(vector<int> v) {
  cout << "Displaying VECTOR" << endl;
  for (int i = 0; i < v.size(); i++)
    cout << v[i] << " ";
  cout << endl;
}

void abc::disp(list<int> l) {
  cout << "Displaying LIST" << endl;
  list<int>::iterator it;

  for (it = l.begin(); it != l.end(); it++)
    cout << *it << "    ";
  cout << endl;
}

void abc::set(list<int> l) {
  list<int>::iterator it;
  for (it = l.begin(); it != l.end(); it++)
    sum_of_squares += (*it) * (*it);
}

// void abc::set(vector<int> v) {
//   for (int i = 0; i < v.size(); i++) {
//     v[i] = v[i] * v[i];
//     sum_of_squares += v[i];
//   }
// }

int abc::get() { return sum_of_squares; }

void abc::set(vector<int> v) {

  for (int i = 0; i < v.size(); i++)
    sum_of_squares += v[i] * v[i];
  sr.SumOfSquares = sum_of_squares;

  sort(v.begin(), v.end());

  sr.min = v[0];
  sr.max = v[v.size() - 1];
  sr.size = v.size();
}

int abc::get_size() { return sr.size; }
int abc::get_max() { return sr.max; }
int abc::get_min() { return sr.min; }
int abc::get_ssq() { return sr.SumOfSquares; }
