#include <list>
#include <vector>
using namespace std;
class abc {
private:
  int sum_of_squares;
  struct Stats_required {
    int SumOfSquares;
    int max;
    int min;
    int size;
  };

  Stats_required sr;

public:
  void print_func();
  abc();
  ~abc();

  vector<int> squared(vector<int> v);
  list<int> squared(list<int> v);
  void disp(vector<int> v);
  void disp(list<int> l);
  void set(vector<int> v);
  void set(list<int> l);
  int get();
  int get_size();
  int get_max();
  int get_min();
  int get_ssq();
};
