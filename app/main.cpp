
#include "../include/containers.h"
#include <iostream>
#include <list>
#include <vector>
int main() {
  int in;
  int size;
  std::cout << "Input Size: ";
  cin >> size;
  vector<int> v1;
  list<int> l1;
  abc c;

  for (int i = 0; i < size; i++) {
    cin >> in;
    v1.push_back(in);
    l1.push_back(in);
  }

  // c.disp(v1);
  // vector<int> v_squared = c.squared(v1);
  // std::cout << "------------" << std::endl;
  // c.disp(v_squared);
  // c.set(v1);

  // c.disp(l1);
  // list<int> l_squared = c.squared(l1);
  // c.disp(l_squared);
  // c.set(l1);
  // std::cout << "sum_of_squares:  " << c.get() << std::endl;

  c.disp(v1);
  c.set(v1);

  std::cout << "Max:  " << c.get_max() << std::endl;
  std::cout << "Min:  " << c.get_min() << std::endl;
  std::cout << "Size:  " << c.get_size() << std::endl;
  std::cout << "sum_of_squares:  " << c.get_ssq() << std::endl;
}